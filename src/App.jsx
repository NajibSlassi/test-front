import './App.css';
import { Route, Routes } from 'react-router-dom';
import { HomePage } from './HomePage';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route exact path="/artwork/:id" element={<HomePage />} />
      </Routes>
    </div>
  );
}

export default App;
