import { useEffect, useMemo, useState } from 'react';
import { useParams } from 'react-router-dom';
import './App.css';
import { Carrousel } from './Carrousel';
import HourglassBottomIcon from '@mui/icons-material/HourglassBottom';
import CheckIcon from '@mui/icons-material/Check';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import PinDropIcon from '@mui/icons-material/PinDrop';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import ViewInArIcon from '@mui/icons-material/ViewInAr';
import StarBorderIcon from '@mui/icons-material/StarBorder';

import './styles.scoped.scss';

export function useIsInViewport(ref) {
  const [isIntersecting, setIsIntersecting] = useState(false);

  const observer = useMemo(
    () => new IntersectionObserver(([entry]) => setIsIntersecting(entry.isIntersecting)),
    []
  );

  useEffect(() => {
    observer?.observe(ref.current);

    return () => {
      observer?.disconnect();
    };
  }, [ref, observer]);

  return isIntersecting;
}

export function HomePage() {
  const { id } = useParams();
  const [contentData, setContentData] = useState(null);

  useEffect(() => {
    fetch('https://storage.googleapis.com/ya-misc/interviews/front/examples/' + id + '.json', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Origin: ''
      }
    })
      .then((response) => response.json())
      .then((data) => setContentData(data));
  }, []);

  return (
    <div className="App">
      <div className="page-container">
        <div className="header-container">
          <div className="header-el">Home &#62;</div>
          <div className="header-el">Painting &#62;</div>
          <div className="header-el">Madeleine Eister</div>
          <div className="header-el">Artworks &#62;</div>
          <div className="header-el">
            <strong>{contentData?.title}</strong>
          </div>
        </div>
        <div className="section-1">
          <div className="image-principale">
            <div>
              <img
                style={{ width: '500px' }}
                className="fit-picture"
                src={contentData?.imageUrl}
                alt="artwork"
              />
            </div>
            <div className="sub-img">
              <RemoveRedEyeIcon style={{ marginLeft: '-30px', position: 'absolute' }} />
              VIEW IN ROOM &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;{' '}
              <ViewInArIcon style={{ marginLeft: '-30px', position: 'absolute' }} />
              AR VIEW
            </div>
            <div className="description-title">Description</div>
            <div className="description-text">{contentData?.description}</div>
            <div className="divider" />
            <div className="description-title">subject, style, medium, materials</div>
            <div className="caracteristiques">
              <div className="fields">
                <div className="element-cara">subject</div>
                <div className="element-cara">style</div>
                <div className="element-cara">medium</div>
                <div className="element-cara">materials</div>
              </div>
              <div className="values">
                <div className="element-cara">{contentData?.subjects?.join(', ')}</div>
                <div className="element-cara">{contentData?.styles?.join(', ')}</div>
                <div className="element-cara">{contentData?.mediums?.join(', ')}</div>
                <div className="element-cara">{contentData?.materials?.join(', ')}</div>
              </div>
            </div>
            <div className="divider" />
          </div>

          <div className="menu-right">
            <div className="title">
              <strong>{contentData?.title} </strong>
            </div>
            <div className="location-1">
              <span className="city">Madeleine Eister</span>
              <span className="france"> France</span>&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
              &nbsp;&nbsp;&nbsp;
              <StarBorderIcon />
            </div>
            <div className="info-1">Painting, 2021</div>
            <div className="info-2">52.2 W x 31.5 H x 1.6 D in</div>
            <div className="price">3,810 EUR</div>
            <div className="acquire">Acquire</div>
            <div className="offer">Make an offer</div>
            <div className="reserve">
              <HourglassBottomIcon /> PRE-RESERVE FOR 24 HOURS
            </div>
            <div className="estimated-delivree-fees">
              <strong>
                <CheckIcon /> 131 EUR estimated delivery fees
              </strong>{' '}
              for France
            </div>
            <div className="reserve">
              In order to obtain an accurate delivery fee, please enter your country of residence
              and zip code.
            </div>
            <div className="location">
              <div className="grey-area">SPAIN</div>
              <div className="grey-area">81932</div>
            </div>
            <div className="delivery-fees">
              <LocalShippingIcon /> Delivery fee is 129 EUR
            </div>
            <div className="info-3">
              <PinDropIcon />
              Free pickup in Bruxelles, Belgium
            </div>
            <div>
              <CheckIcon />
              Try 14 days at home fo free
            </div>
          </div>
        </div>
        {contentData && <Carrousel contentData={contentData} />}
      </div>
    </div>
  );
}
