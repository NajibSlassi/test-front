import { object } from 'prop-types';
import { useEffect, useMemo, useRef, useState } from 'react';
import Slider from 'react-slick';
import './App.css';

import './styles.scoped.scss';

export function useIsInViewport(ref) {
  const [isIntersecting, setIsIntersecting] = useState(false);

  const observer = useMemo(
    () => new IntersectionObserver(([entry]) => setIsIntersecting(entry.isIntersecting)),
    []
  );

  useEffect(() => {
    observer?.observe(ref.current);

    return () => {
      observer?.disconnect();
    };
  }, [ref, observer]);

  return isIntersecting;
}

Carrousel.propTypes = {
  contentData: object
};

export function Carrousel({ contentData }) {
  const slider = useRef(null);

  const first = useRef();
  const last = useRef();

  const isInViewFirst = useIsInViewport(first);
  const isInViewLast = useIsInViewport(last);

  const settings = {
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    draggable: false,
    className: 'react__slick__slider__parent',
    arrows: false,
    infinite: false
  };

  const component = (
    <Slider ref={slider} {...settings}>
      {contentData?.otherArtworkImages.map((imgSrc, index) => {
        let refToAdd;
        if (index === 0) {
          refToAdd = first;
        }

        if (index === contentData?.otherArtworkImages?.length - 1) {
          refToAdd = last;
        }
        return (
          <div key={index} ref={refToAdd}>
            <img style={{ maxHeight: '150px' }} src={imgSrc} />
          </div>
        );
      })}
    </Slider>
  );

  if (first?.current?.clientWidth === 0) window.location.reload(false);

  return (
    <div className="carrousel-container">
      {!isInViewFirst && (
        <button style={{ backgroundColor: 'white', border: 'none' }}>
          <span
            style={{ cursor: 'pointer' }}
            onClick={() => slider?.current?.slickPrev()}
            className="chevron left"></span>
        </button>
      )}
      <div className="carrousel">{component}</div>
      {!isInViewLast && (
        <button style={{ backgroundColor: 'white', border: 'none' }}>
          <span
            style={{ cursor: 'pointer' }}
            onClick={() => slider?.current?.slickNext()}
            className="chevron right"></span>
        </button>
      )}
    </div>
  );
}
